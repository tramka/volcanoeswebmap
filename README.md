# VolcanoesWebMap

## Description
Project creates simple map of volcanoes in the United States. They are divided into 3 categories by their height (3 colors - red, orange, light green). The background of the map represents countries population (also divided into 3 categories).

## Motivation
Project was created as a hobby project during the course Build 10 Real World Applications within Udemy platform. The course was held in online form. More information about the course: https://www.udemy.com/the-python-mega-course.

## Installation
You need Python 3 to run program.

To make script work, clone the repository. After that, install requirements:
``pip install -r requirements.txt``

## Usage
To run the program, use:

``python map_volcanoes.py``

Script creates a HTML file in main folder. File can be open locally.

### Example:
![Visualization 1](example1.png)

## Contributing
Any ideas how to improve project is welcomed. You can create issue here or write message to the email.

## Credits
Created by Tomas Cernak.

Email: cernak.tomi@gmail.com

Released: 7.11.2018

## To do
Legend
