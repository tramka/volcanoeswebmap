import folium
import pandas

data = pandas.read_csv('Volcanoes.txt')
lat = list(data['LAT'])
lon = list(data['LON'])
elev = list(data['ELEV'])
name = list(data['NAME'])
type = list(data['TYPE'])

def color_producer(el):
    if el < 1500:
        return 'lightgreen'
    elif 1500 <= el < 3000:
        return 'orange'
    else:
        return 'red'

# creates background layers
map = folium.Map(location=[40.60, -106.88], zoom_start=5, tiles="Mapbox Bright")
folium.TileLayer('openstreetmap').add_to(map)
folium.TileLayer('stamenterrain').add_to(map)
folium.TileLayer('cartodbpositron').add_to(map)

# creates layers with added information (volcanoes, population)
fg_vol = folium.FeatureGroup(name="Volcanoes")
fg_pop = folium.FeatureGroup(name="Population")

# popup formatting
html = """<h5>Volcano information:</h5>
<div style="font-size:12px;"> Name:
<a href="https://www.google.com/search?q={}" target="_blank">{}</a><br>
Type: {}<br>
Height: {} m</div>
"""
# filling up volcanoes layer
for lt, ln, el, tp, nm in zip(lat, lon, elev, type, name):
    text_popup = html.format(nm, nm, tp, el)
    color = color_producer(el)
    iframe = folium.IFrame(html=text_popup, width=200, height=100)
    fg_vol.add_child(folium.CircleMarker(location =[lt,ln],
                            radius=6,
                            popup=folium.Popup(iframe),
                            fill_color = color,
                            color="grey",
                            fill=True,
                            fill_opacity=0.8))

# filling up population layer
fg_pop.add_child(folium.GeoJson(data=open('world.json', 'r', encoding='utf-8-sig').read(),
style_function = lambda x: {'fillColor': 'green' if x['properties']['POP2005'] < 10000000
else 'orange' if 10000000 <= x['properties']['POP2005'] < 20000000 else 'red'}))

map.add_child(fg_pop)
map.add_child(fg_vol)
map.add_child(folium.LayerControl())

map.save("Map1.html")
